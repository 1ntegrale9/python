__author__  = "1ntegrale9"
__date__    = "2017_04_07"

# How to use
# $ python3 ReadComment.py AbsolutePathOfDirectory"

import os
import sys
import locale
import MeCab

def simple_chardet(b_str):
    try:
        b_str.decode ('us-ascii')
        return 'us-ascii'
    except UnicodeDecodeError:
        try:
            b_str.decode ('utf-8')
            return 'utf-8'
        except UnicodeDecodeError:
            try:
                b_str.decode ('cp932')
                return 'cp932'
            except UnicodeDecodeError:
                try:
                    b_str.decode ('eucjp')
                    return 'eucjp'
                except UnicodeDecodeError:
                    return None

def printchardet(infile):
    locale.setlocale (locale.LC_ALL, '')
    maxsize = 1 * 1024 * 1024

    try:
        with open (infile, 'rb') as f_in:
            try:
                b_text = f_in.read (maxsize)
            except IOError as e:
                sys.exit ('Error: cannot read from file "{0}" [{1}]: {2}'.format (infile, e.errno, e.strerror))
    except IOError as e:
        sys.exit ('Error: cannot open file "{0}" [{1}]: {2}'.format (infile, e.errno, e.strerror))

    encoding = simple_chardet (b_text)
    return (encoding if encoding else 'other encoding')

# データ読み込み
def fileinput(filename,encoding):
    with open(filename,mode = 'r',encoding=encoding) as file:
        return(file.read())

# データ書き込み
def fileoutput(filename,encoding,text):
    with open(filename,mode = 'a',encoding=encoding) as file:
        file.write("\n" + text)

# 空白と改行の除去
def BlankRemove(text):
    return("".join(text.split()))

def CheckPathType(path):
    if os.path.isfile(path):
        pathType = "file"
    elif os.path.isdir(path):
        pathType = "dir"

def RecursiveFullSearch(rootPath,query):
    if os.path.isdir(rootPath) == True:
        for childPath in os.listdir(rootPath):
            RecursiveFullSearch(rootPath+childPath+"/",query)
    elif os.path.isfile(rootPath):
        WordSearch(rootPath,query)

def WordSearch(filePath,query):
    fileChardet = printchardet(filePath)
    if fileChardet in ['utf-8','cp932','eucjp','us-ascii']:
        print(str(os.path.dirname(filePath)).replace("/","_")).replace(".","_")
        fileoutput(str(os.path.dirname(filePath)).replace("/","_").replace(".","_") + ".md",'UTF-8',"## " + os.path.abspath(filePath))
        fileTextLines = fileinput(filePath,fileChardet).splitlines()
        for lineNumber,textLine in enumerate(fileTextLines):
            print(str(os.path.dirname(filePath)).replace("/","_")).replace(".","_")
            fileoutput(str(os.path.dirname(filePath)).replace("/","_").replace(".","_") + ".md",'UTF-8',str(lineNumber) + "\t" + str(textLine))
            if query in str(textLine):
                print(os.path.abspath(filePath))
                print("line " + str(lineNumber+1))
                print(textLine)
                print("")
    elif ".png" not in filePath:
        pass

RecursiveFullSearch(sys.argv[1],"schedule")