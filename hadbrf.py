import json

#データ変更ファイル
datafile = "sample.json"

# データ読み込み
def fileinput(filename,encoding):
    with open(filename,'r',encoding=encoding) as file:
        return(json.load(file))

#データ上書き
def fileoutput(filename,encoding,data):
    with open(filename,'w',encoding=encoding) as file:
        json.dump(data,file,sort_keys = True, indent = 4)

def HAReformat(oldHADB):
    newHADB = {'0': {'date':{'year':"0",'month':"0",'day':"0"},'item':{'kind':"0",'place':"0",'amount':"0"},'record':{'year':"0",'month':"0",'day':"0"}}}

    for i in range(len(oldHADB)):
        oldrecord = oldHADB[str(i)]
        olddata = oldrecord['data']

        year = olddata['year']
        month = olddata['month']
        day = olddata['day']
        date = {'year':year,'month':month,'day':day}

        amount = olddata['amount']
        kind = olddata['kind']
        place = olddata['place']
        item = {'kind':kind,'place':place,'amount':amount}

        thisyear = oldrecord['year']
        thismonth = oldrecord['month']
        thisday = oldrecord['day']
        record = {'year':thisyear,'month':thismonth,'day':thisday}

        newHADB[str(i)] = {'date':date,'item':item,'record':record}

    return(newHADB)

oldHADB = fileinput(datafile,'utf-8')
newHADB = HAReformat(oldHADB)
fileoutput(datafile,'utf-8',newHADB)