__author__  = "1ntegrale9"
__date__    = "2017_04_07"

# How to use
# $ python3 ReadComment.py AbsolutePathOfDirectory"

import os
import sys
import locale
import string

def simple_chardet(b_str):
    try:
        b_str.decode ('iso-2022-jp')
        return '7bit (ascii, iso-2022-jp, etc...)'
    except UnicodeDecodeError:
        try:
            b_str.decode ('utf-8')
            return 'utf-8'
        except UnicodeDecodeError:
            try:
                b_str.decode ('cp932')
                return 'cp932'
            except UnicodeDecodeError:
                try:
                    b_str.decode ('eucjp')
                    return 'eucjp'
                except UnicodeDecodeError:
                    return None

def printchardet(infile):
    locale.setlocale (locale.LC_ALL, '')
    maxsize = 1 * 1024 * 1024

    try:
        with open (infile, 'rb') as f_in:
            try:
                b_text = f_in.read (maxsize)
            except IOError as e:
                sys.exit ('Error: cannot read from file "{0}" [{1}]: {2}'.format (infile, e.errno, e.strerror))
    except IOError as e:
        sys.exit ('Error: cannot open file "{0}" [{1}]: {2}'.format (infile, e.errno, e.strerror))

    encoding = simple_chardet (b_text)
    return (encoding if encoding else 'other encoding')

# データ読み込み
def fileinput(filename,encoding):
    with open(filename,mode = 'r',encoding=encoding) as file:
        return(file.read())

# データ書き込み
def fileoutput(filename,encoding,text):
    with open(filename,mode = 'a',encoding=encoding) as file:
        file.write("\n" + text)

def dirname(text):
    return str(text).replace(".","").replace("/","_")

def main(f):
    file = f
    output = "MD" + file
    c = printchardet(file)
    if c == 'utf-8' or c == 'cp932' or c == 'eucjp':
        for line in fileinput(file,c).splitlines():
            if "##" in line:
                fileoutput(output,'UTF-8',line)
            elif len(line) < 4:
                if "*" in line or "/" in line:
                    fileoutput(output,'UTF-8',"    " + line)
                else:
                    fileoutput(output,'UTF-8',"")
            elif line[0:4] == "    ":
                fileoutput(output,'UTF-8',line)

for i in os.listdir("."):
    if ".md" in i:
        main(i)