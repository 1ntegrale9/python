__author__  = "1ntegrale9"
__date__    = "2017_04_07"

# How to use
# $ python3 ReadComment.py AbsolutePathOfDirectory"

import os
import sys
import locale

def simple_chardet(b_str):
    try:
        b_str.decode ('iso-2022-jp')
        return '7bit (ascii, iso-2022-jp, etc...)'
    except UnicodeDecodeError:
        try:
            b_str.decode ('utf-8')
            return 'utf-8'
        except UnicodeDecodeError:
            try:
                b_str.decode ('cp932')
                return 'cp932'
            except UnicodeDecodeError:
                try:
                    b_str.decode ('eucjp')
                    return 'eucjp'
                except UnicodeDecodeError:
                    return None

def printchardet(infile):
    locale.setlocale (locale.LC_ALL, '')
    maxsize = 1 * 1024 * 1024

    try:
        with open (infile, 'rb') as f_in:
            try:
                b_text = f_in.read (maxsize)
            except IOError as e:
                sys.exit ('Error: cannot read from file "{0}" [{1}]: {2}'.format (infile, e.errno, e.strerror))
    except IOError as e:
        sys.exit ('Error: cannot open file "{0}" [{1}]: {2}'.format (infile, e.errno, e.strerror))

    encoding = simple_chardet (b_text)
    return (encoding if encoding else 'other encoding')

def fileinput(filename,encoding):
    with open(filename,encoding=encoding) as file:
        return(file.read())

def printtitle(text):
    [print("-",end="") for _ in range(100)]
    print("")
    print(text)
    [print("-",end="") for _ in range(100)]
    print("")

def main():
    dir = sys.argv[1]
    fps = [fp for fp in os.listdir(dir)]
    for f in fps:
        printtitle(dir+f)
        if os.path.isfile(dir+f):
            c = printchardet(dir+f)
            if c == 'utf-8' or c == 'cp932' or c == 'eucjp':
                ftl = fileinput(dir+f,c)
                flg = 0
                for ft in ftl.splitlines():
                    if "/*" in ft:
                        flg = 1
                    if flg == 1:
                        print(ft)
                    if "*/" in ft:
                        flg = 0
                        print("")
        print("\n Press the Enter key to proceed.",end="")
        input()

main()