# coding:utf-8

# 指定したディレクトリ内のディレクトリ&ファイル名一覧を出力する
# 第一引数or標準入力でディレクトリパスを受け取る
from __future__ import print_function
import os
import sys

# ディレクトリパスを受け取る
try:
    dirPath = sys.argv[1]
except:
    print("Please enter the directory path.")
    dirPath = input()

# リスト出力時に逐一改行するか空白を挟むか
try:
    if sys.argv[2] == "nlb":
        endCode = " "
    else:
        endCode = "\n"
except:
    endCode = "\n"

# リストを出力する
fileNameList = os.listdir(dirPath)
for fileName in fileNameList:
    print(fileName, end=endCode)

# 最後に改行されない場合は改行する
if (endCode == " "):
    print("")
