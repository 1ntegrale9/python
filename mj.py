__author__ = "1ntegrale9"
__version__ = "0.0.1"
__date__    = "2017_04_06"

import json
import datetime
import locale

#データ格納ファイル
datafile = 'sample.json'

#初期辞書
initialdic = {}

#改行
def LineBreak():
    print("")

#データ読み込み
def fileinput(filename,encoding):
    with open(filename,'r',encoding=encoding) as file:
        return(json.load(file))

#データ上書き
def fileoutput(filename,encoding,data):
    with open(filename,'w',encoding=encoding) as file:
        json.dump(data,file,sort_keys = True, indent = 4)

#データ追加
def UpdateDictionary(stdin):
    dicLayer = stdin.split()
    element = dicLayer.pop()
    while(len(dicLayer) != 0):
        element = {dicLayer.pop():element}
    #追加する辞書
    newdata = element
    #家計簿データベースに新しいデータを追加
    DB = fileinput(datafile,'utf-8')
    numbering = str(len(DB))
    DB[numbering] = newdata
    #json形式で保存
    fileoutput(datafile,'utf-8',DB)
    print("\ndata appended.")

while True:
    stdin = input()
    if stdin == "view": #データ一覧表示
        print(json.dumps(fileinput(datafile,'utf-8'),sort_keys = True, indent = 4))
    elif stdin == "clear": #データ初期化
        fileoutput(datafile,'utf-8',initialdic)
    elif len(stdin) > 1: #データ追加
        UpdateDictionary(stdin)
