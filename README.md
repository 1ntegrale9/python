# Python (This Repository)
これらはPython3形式の汎用ツール群です。  
冗長ですが1コードのみで実行可能にするために全く同じコードや関数が含まれている場合があります。  
  
## Comment2md.py  
指定されたディレクトリ内の全ファイルの/* */で囲われたコメントを集約してMarkdown形式で出力します。  
第一引数にディレクトリのパスを指定してください。  
  
## SearchAll.py
指定したキーワードが含まれるディレクトリ以下の全てのファイルを出力します。  
  
## hadb.py
家計簿をjson形式で保存します。

## hadbrf.py
旧hadb.pyで出力されたjson形式ファイルを最新hadb.pyのjson形式に変換します。

## mdPutInOrder.py
実行したディレクトリ内のMarkdownファイルを整形します。  
Comment2md.pyで出力したMarkdown用です。  
  
## mj.py
標準入力からメモを受け取りjson形式で保存します。  
  
# Future implementation
- import xxx をソースコードに置き換える(単一で実行できるファイルにする)プログラム
- ディレクトリ下の .DS_Store を全て削除するプログラム
